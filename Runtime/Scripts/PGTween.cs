using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static System.Net.Mime.MediaTypeNames;

public static class PGTween
{
    public static async void OnMaterialValueTween(this Material material, string parameter, float to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        float elapsedTime = 0f;
        float from = material.GetFloat(parameter);

        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        while (elapsedTime < time)
        {
            material.SetFloat(parameter, Mathf.LerpUnclamped(from, to, animationCurve.Evaluate(elapsedTime / time)));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        material.SetFloat(parameter, to);
    }
    public static async void OnMaterialValueTween(this Material material, string parameter, Color to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        float elapsedTime = 0f;
        Color from = material.GetColor(parameter);
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        while (elapsedTime < time)
        {
            material.SetColor(parameter, Color.LerpUnclamped(from, to, animationCurve.Evaluate(elapsedTime / time)));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        material.color = to;
    }

    public static async void OnAlphaTween(this CanvasGroup canvasGroup, float to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        float elapsedTime = 0f;
        float from = canvasGroup.alpha;
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        while (elapsedTime < time)
        {
            if (canvasGroup == null)
            {

            }
            canvasGroup.alpha = Mathf.LerpUnclamped(from, to, animationCurve.Evaluate(elapsedTime / time));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        canvasGroup.alpha = to;
    }
    public static async void OnAlphaTween(this Image image, float to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        Color from = image.color;
        while (elapsedTime < time)
        {
            image.color = Color.LerpUnclamped(from, new Color(image.color.r, image.color.r, image.color.r, to), animationCurve.Evaluate(elapsedTime / time));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        image.color = new Color(image.color.r, image.color.r, image.color.r, to);
    }
    public static async void OnAlphaTween(this TMP_Text tmpText, float to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        float from = tmpText.alpha;
        while (elapsedTime < time)
        {
            tmpText.alpha = Mathf.LerpUnclamped(from, to, animationCurve.Evaluate(elapsedTime / time));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        tmpText.alpha = to;
    }
    public static async void OnAlphaTween(this Material material, float to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        float elapsedTime = 0f;
        Color from = material.color;
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        while (elapsedTime < time)
        {
            material.color = Color.LerpUnclamped(from, new Color(material.color.r, material.color.r, material.color.r, to), animationCurve.Evaluate(elapsedTime / time));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        material.color = new Color(material.color.r, material.color.r, material.color.r, to);
    }

    public static async void OnColorTween(this Image image, Color to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        float elapsedTime = 0f;
        Color from = image.color; // Запоминаем начальный цвет

        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        while (elapsedTime < time)
        {

            float t = elapsedTime / time; // Вычисляем прогресс от 0 до 1

            // Интерполируем цвет между начальным и целевым цветом
            image.color = Color.LerpUnclamped(from, to, animationCurve.Evaluate(t));

            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        image.color = to;
    }
    public static async void OnColorTween(this Material material, Color to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        float elapsedTime = 0f;
        Color from = material.color; // Запоминаем начальный цвет

        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        while (elapsedTime < time)
        {
            if (material == null)
            {

            }
            float t = elapsedTime / time; // Вычисляем прогресс от 0 до 1

            // Интерполируем цвет между начальным и целевым цветом
            material.color = Color.LerpUnclamped(from, to, animationCurve.Evaluate(t));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        material.color = to;
    }

    public static async void OnTransformTween(this Transform transform, Vector3 to, float time, bool useIgnoreTimeScale = false, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        Vector3 from = transform.position;
        while (elapsedTime < time)
        {
            if (transform == null)
            {

            }
            transform.position = Vector3.LerpUnclamped(from, to, animationCurve.Evaluate(elapsedTime / time));
            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
            await Task.Yield();
        }
        transform.position = to;
    }

    public static async void OnValueTween(float from, float to, float time, bool useIgnoreTimeScale = false, System.Action<float> onUpdate = default, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            float t = elapsedTime / time;
            float value = Mathf.LerpUnclamped(from, to, animationCurve.Evaluate(t));
            onUpdate?.Invoke(value);

            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }

            await Task.Yield();

            // Проверяем, существует ли объект onUpdate


        }

        // Убедимся, что последнее значение соответствует точному значению "to"
        onUpdate.Invoke(to);
    }
    public static async void OnValueTween(Quaternion from, Quaternion to, float time, bool useIgnoreTimeScale = false, System.Action<Quaternion> onUpdate = default, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            float t = elapsedTime / time;
            Quaternion value = Quaternion.LerpUnclamped(from, to, animationCurve.Evaluate(t));
            onUpdate?.Invoke(value);

            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }

            await Task.Yield();

            // Проверяем, существует ли объект onUpdate


        }

        // Убедимся, что последнее значение соответствует точному значению "to"
        onUpdate.Invoke(to);
    }

    public static async void OnValueTween(Vector3 from, Vector3 to, float time, bool useIgnoreTimeScale = false, System.Action<Vector3> onUpdate = default, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            float t = elapsedTime / time;
            Vector3 value = Vector3.LerpUnclamped(from, to, animationCurve.Evaluate(t));
            onUpdate?.Invoke(value);

            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }

            await Task.Yield();

        }

        // Убедимся, что последнее значение соответствует точному значению "to"
        onUpdate.Invoke(to);
    }

    public static async void OnValueTween(Vector2 from, Vector2 to, float time, bool useIgnoreTimeScale = false, System.Action<Vector2> onUpdate = default, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            float t = elapsedTime / time;
            Vector2 value = Vector2.LerpUnclamped(from, to, animationCurve.Evaluate(t));
            onUpdate?.Invoke(value);

            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }

            await Task.Yield();

            // Проверяем, существует ли объект onUpdate


        }

        // Убедимся, что последнее значение соответствует точному значению "to"
        onUpdate.Invoke(to);
    }

    public static async void OnValueTween(Color from, Color to, float time, bool useIgnoreTimeScale = false, System.Action<Color> onUpdate = default, AnimationCurve animationCurve = null)
    {
        if (animationCurve == null)
        {
            animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            float t = elapsedTime / time;
            Color value = Color.LerpUnclamped(from, to, animationCurve.Evaluate(t));
            onUpdate?.Invoke(value);

            if (useIgnoreTimeScale)
            {
                elapsedTime += Time.unscaledDeltaTime;
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }

            await Task.Yield();

            // Проверяем, существует ли объект onUpdate


        }

        // Убедимся, что последнее значение соответствует точному значению "to"
        onUpdate.Invoke(to);
    }

}
